import {command} from "./camera-api";

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const {savePreset} = require('./camera-api');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

app.post('/action', (req, res) => {
    const body = req.body;
    return command(body).then(() => {
        res.json('SUCCESS');
    }).catch(() => {
        res.json('ERROR');
    });
});
module.exports = app;