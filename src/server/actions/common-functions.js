export function replacer(string, regex, to) {
    if (string.indexOf(regex) !== -1) {
        string = string.replace(regex, to);
        return replacer(string, regex, to);
    }
    return string;
}

export const ACTIONS = {
    WEB_RequestSessionIDAPI: 'WEB_RequestSessionIDAPI',
    WEB_RequestCertificateAPI: 'WEB_RequestCertificateAPI',
    WEB_CtrlCameraOpeateAPI: 'WEB_CtrlCameraOpeateAPI',
    WEB_GetMailboxDataAPI: 'WEB_GetMailboxDataAPI',
};

