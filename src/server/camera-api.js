const request = require('request');
const {ACTIONS, replacer} = require('./actions/common-functions');
let sessionID;

const type = {
    SAVE: 'SAVE',
    GET: 'GET'
};

export function command(params) {
    if (params.host.indexOf('https://') === -1) {
        params.host = 'https://' + params.host;
    }
    if (params.host.indexOf('action.cgi') === -1) {
        params.host = params.host + '/action.cgi?ActionID=';
    }
    return generateSessionId(params);
}

function generateSessionId(params) {
    return new Promise((resolve, reject) => {
        request({
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            uri: params.host + ACTIONS.WEB_RequestSessionIDAPI,
            method: 'POST'
        }, function (error, res) {
            if (error) {
                return reject(error);
            } else {
                const {statusCode, body} = res;
                console.log(body);
                if (statusCode === 200) {
                    let {success, data} = JSON.parse(body);
                    if (success === 1) {
                        data = JSON.parse(replacer(data, '\'', '"'));
                        const {acSessionId, ...props} = data;
                        params.sessionID = acSessionId;
                        resolve(authorization(params));
                    }
                }
                return reject(error);
            }
        });
    });
}

function authorization(params) {
    const {user, password, host, sessionID} = params;
    const formData = JSON.stringify({user, password});
    const contentLength = formData.length;
    return new Promise((resolve, reject) => {
        request({
            headers: {
                'Content-Length': contentLength,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Connection': 'Keep-Alive',
                'Cookie': `SessionID=${sessionID}`
            },
            uri: host + ACTIONS.WEB_RequestCertificateAPI,
            body: formData,
            method: 'POST'
        }, function (err, res, body) {
            if (err) reject(err);
            const bodyJson = JSON.parse(body);
            if (bodyJson.success === 1) {
                resolve(cameraControl(params));
            }
            return reject(err);
        });
    });
}

function cameraControl(params) {
    const form = {
        camState: params.camState,
        camAction: parseInt(params.camAction),
        camPos: parseInt(params.camPos),
        camSrc: parseInt(params.camSrc)
    };
    const formData = JSON.stringify(form);
    const contentLength = formData.length;
    return new Promise((resolve, reject) => {
        request({
            headers: {
                'Content-Length': contentLength,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Cookie': `SessionID=${params.sessionID}`
            },
            uri: params.host + ACTIONS.WEB_CtrlCameraOpeateAPI,
            body: formData,
            method: 'POST'
        }, function (err, res, body) {
            if (err) {
                reject(err);
            } else if (body && JSON.parse(body).success === 1) {
                resolve('SUCCESS');
            }
            reject('ERROR');
        });
    })
}