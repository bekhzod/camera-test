import {httpServer} from './src/http-server';

const {HTTP_PORT = '9999'} = process.env;

httpServer.listen(HTTP_PORT, () => {
    console.info(`HTTP server listening on port ${HTTP_PORT}.`);
});